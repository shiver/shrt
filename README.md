## Build

Requires nightly Rust version nightly-2017-05-24-x86_64-unknown-linux-gnu.
Newer versions break rustfmt, clippy, or diesel.

```
$ rustup override set nightly 2017-05-24
```

## Usage

Create a new Shrt URL redirect:

```
Request: PUT /url Content-Type: application/json
    {
        long_url: "http://goole.com"
    }

Response:
    {
        domain: "sh.rt",
        token: "Grqh9ncka",
        shrt_url: "sh.rt/Grqh9ncka",
        redirect_url: "http://goole.com",
        redirect_status: 307,
        user_data: null,
        created: "2017-05-28 23:56:04.336425",
        stats: {
            hit_count: 0,
            last_activity: null
        }
    }

```


## Example nginx proxy forwarding setup

```
location / {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header X-NginX-Proxy true;
    proxy_pass http://localhost:8000;
    proxy_redirect off;
}
```

## Todo

- [X] Random BASE62 token generation, encoding and decoding. (2^29)
- [X] Persist token information
- [ ] Timed token expiry, with optional automatic extension on access.
- [ ] Optional fallback redirect URL for "silent" failure. This should be configurable per "namespace".
- [ ] Optional nonce values injected into rediret URL params. [token + user provided identifier + server time]
  - Nonce construction:
  ```
  {
      protocol_id: u16,
  }
  ```
- [ ] Optional user supplied identifier encoded with a random salt injected into the redirect URL params.
- [X] Deactivated tokens should behave as failure.
- [ ] Restricted endpoints for the following operations:
  - [X] POST /token { url: "http://google.com" } - Generate redirect with new random token with some entrophy.
  - [X] POST /token { token: "test", url: "http://google.com" }  - Generate redirect with user defined token.
  - [X] GET /token - Redirect to the relevant URL, or "fail" silently to a globally defined fallback.
  - [ ] GET /<project>/<token> - Redirect to the relevant URL, or "fail" silently to a namespace defined fallback.
- [ ] Create default domain on start up if it doesn't already exist.
- [ ] If no project, and failed redirect, fallback to default domain.
- [ ] If project and failed redirect, fallback to project default.


- [ ] Shrt link data definition
```
{
    pk: 1
    uuid: "c06184d9-c4b7-44d0-8505-4117d9d8a25b"        // Used when referencing via API
    decoded: 3683080414263560                           // Crytographically secure random identifier
    token: "Grqh9ncka"                                  // Base62 encoded representation of the decoded identifier above.
    shrt_url: "shrt.rs/Grqh9ncka"
    domain: "shrt.rs"
    redirect_url: "google.com"
    redirect_status: 307                                // HTTP status code to use in the redirect (307 default)
    inject_nonce: true                                  // Append identifier to redirected URL
    active: true,                                       // false for "deleted"
    expire_after_inactivty: 7776000                     // Expires after 90 days (expressed in seconds)
    created: "2011-01-01T00:00"
    updated: "2011-01-01T00:01"
}
```

- [ ] Data points to be tracked on redirect request
  - [X] Token
  - [X] Request host/domain
  - [X] Referrer
  - [X] User agent string (OS / Browser)
  - [X] Real client address (X-Forwarded-For)
  - [X] Time of event
  - [X] *debug* Response generation time
  - [ ] *debug* Decoded token
  - [ ] *debug* Redirect destination

### Nice to have
- [ ] goo.gl API emulation if possible
- [ ] Bind tokens based on hosting domain. Allows for duplicate tokens across domain.
- [ ] Frontend management and reporting
- [ ] Redis caching layer
- [P] error_chain
- [ ] Callback to external site to supply custom redirect or inject some dynamic data into the redirect
