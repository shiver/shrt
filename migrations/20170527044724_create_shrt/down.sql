-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS tracks;
DROP TABLE IF EXISTS shrts;
DROP TABLE IF EXISTS projects;
DROP TABLE IF EXISTS domains;
DROP EXTENSION pgcrypto;
