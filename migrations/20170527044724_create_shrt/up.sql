CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE domains (
    id SERIAL PRIMARY KEY,
    fqdn VARCHAR NOT NULL,
    fallback_url VARCHAR,
    created TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
    updated TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc')
);

CREATE TABLE projects (
    id SERIAL PRIMARY KEY,
    domain_id INTEGER NOT NULL REFERENCES domains(id),
    path VARCHAR UNIQUE NOT NULL,
    fallback_url VARCHAR,
    description TEXT,
    created TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
    updated TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc')
);

CREATE TABLE shrts (
    id SERIAL PRIMARY KEY,
    token VARCHAR NOT NULL,
    -- decoded NUMERIC,
    -- uuid UUID UNIQUE NOT NULL DEFAULT gen_random_uuid(),
    shrt_url VARCHAR NOT NULL,
    path_id INTEGER REFERENCES projects(id),
    domain_id INTEGER REFERENCES domains(id),
    redirect_url VARCHAR NOT NULL,
    redirect_status INTEGER DEFAULT 307,
    user_data BIGINT,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    expire_after_inactivity INTEGER DEFAULT 7776000,
    created TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
    updated TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc')
);

CREATE INDEX ON shrts(token);
CREATE INDEX ON shrts(token, active);
CREATE UNIQUE INDEX ON shrts(token, active, path_id, domain_id);

CREATE TABLE tracks (
    id SERIAL PRIMARY KEY,
    shrt_id INTEGER NOT NULL REFERENCES shrts(id),
    token VARCHAR NOT NULL,
    -- decoded NUMERIC,
    host VARCHAR,
    shrt_url VARCHAR NOT NULL,
    redirect_url VARCHAR NOT NULL,
    injected_params VARCHAR,
    user_agent VARCHAR,
    referrer VARCHAR,
    client_addr VARCHAR,
    gen_time INTEGER,
    created TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc')
);

CREATE INDEX ON tracks(shrt_id, created);
CREATE INDEX ON tracks(shrt_id);


