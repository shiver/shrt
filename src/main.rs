#![feature(plugin)]
#![feature(test)]
#![plugin(rocket_codegen)]
#![recursion_limit = "1024"]

extern crate rocket;

#[macro_use]
extern crate rocket_contrib;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

extern crate dotenv;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate rand;
extern crate base62;
extern crate uuid;
extern crate chrono;
#[macro_use]
extern crate error_chain;

extern crate test;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;


use std::env;
use std::time::Instant;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use r2d2_diesel::ConnectionManager;
use r2d2::ManageConnection;
use rand::distributions::{IndependentSample, Range};
use base62::BigUint;
use base62::PrimInt;

use rocket::{Outcome, State};
use rocket::request::{self, FromRequest, Request};
use rocket::response::Redirect;
use rocket::http::{Cookies, Status};
use rocket_contrib::{JSON, Value};

use chrono::NaiveDateTime;

use self::guard::ClientDetails;
use self::models::{Shrt, NewShrt, NewTrack};
use self::db::{get_shrt_from_token, gen_shrt_from_form, store_track, deactivate_shrt,
               get_hit_count, get_last_activity, get_project_by_path, ShrtForm};

// TODO: Move forms into forms.rs?

pub mod schema;
pub mod models;
pub mod guard;
pub mod db;

pub mod errors {
    error_chain!{}
}

use errors::*;

const NONCE_PROTO_VERSION: u8 = 1;
const DEFAULT_DOMAIN: &'static str = "__DEFAULT__";

pub struct Config {
    db_pool: r2d2::Pool<ConnectionManager<PgConnection>>,
}

#[get("/<token>")]
fn handle_token(token: &str, client: ClientDetails, config: State<Config>) -> Redirect {
    let start = Instant::now();
    let shrt = match get_shrt_from_token(&token, None, &config) {
        Ok(s) => s,
        Err(e) => {
            println!("{:#?}", e);
            return Redirect::temporary("/");
        }
    };

    let url = format!("{}", &shrt.redirect_url);
    store_track(&start, &url, &client, &shrt, &config);
    Redirect::temporary(&shrt.redirect_url)
}

#[get("/<path>/<token>", rank = 2)]
fn handle_project_token(path: &str,
                        token: &str,
                        client: ClientDetails,
                        config: State<Config>)
                        -> Redirect {
    let start = Instant::now();
    let project = match get_project_by_path(&path, &config) {
        Ok(p) => p,
        Err(e) => {
            println!("{:#?}", e);
            return Redirect::temporary("/");
        }
    };

    let shrt = match get_shrt_from_token(&token, Some(project.id), &config) {
        Ok(s) => s,
        Err(e) => {
            println!("{:#?}", e);
            return Redirect::temporary("/");
        }
    };

    let url = format!("{}", &shrt.redirect_url);
    store_track(&start, &url, &client, &shrt, &config);
    Redirect::temporary(&shrt.redirect_url)

}

#[get("/url/<token>", rank = 1)]
fn get_shrt(token: &str, config: State<Config>) -> JSON<Value> {
    let shrt = match get_shrt_from_token(&token, None, &config) {
        Ok(s) => s,
        Err(e) => return JSON(json!({"status": "error"})),
    };

    let hit_count = match get_hit_count(shrt.id, &config) {
        Ok(h) => Some(h),
        _ => None,
    };

    let last_hit = match get_last_activity(shrt.id, &config) {
        Ok(a) => Some(a.last_hit.unwrap().to_string()),
        _ => None,
    };

    JSON(json!({
        "token": &shrt.token,
        "shrt_url": &shrt.shrt_url,
        "domain_id": &shrt.domain_id,
        "path_id": &shrt.path_id,
        "redirect_url": &shrt.redirect_url,
        "redirect_status": &shrt.redirect_status,
        "user_data": &shrt.user_data,
        "stats": {
            "hit_count": &hit_count,
            "last_hit": &last_hit
        },
        "created": &shrt.created.and_then(|v| Some(v.to_string()))
    }))
}

#[put("/url", format = "application/json", data = "<form>")]
fn create_shrt(form: JSON<ShrtForm>, config: State<Config>) -> JSON<Value> {
    let shrt = gen_shrt_from_form(&form, &config);

    if let Ok(shrt) = shrt {
        JSON(json!({
            "shrt_url": &shrt.shrt_url,
            "redirect_url": &shrt.redirect_url
        }))
    } else {
        JSON(json!({"status": "error"}))
    }
}

#[post("/url", format = "application/json", data = "<form>")]
fn update_shrt(form: JSON<ShrtForm>, config: State<Config>) -> JSON<Value> {
    JSON(json!({
        "status": "success"
    }))
}

#[delete("/url/<token>")]
fn delete_shrt(token: &str, config: State<Config>) -> JSON<Value> {
    let conn = config.db_pool.clone().get().unwrap();
    deactivate_shrt(&token, &conn);
    JSON(json!({
        "status": "success"
    }))
}

pub fn establish_connection<T: Connection + 'static>() -> r2d2::Pool<ConnectionManager<T>> {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let config = r2d2::Config::default();
    let manager = ConnectionManager::<T>::new(database_url);
    r2d2::Pool::new(config, manager).expect("Failed to create pool.")
}

fn rand_token() -> (u32, String) {
    let mut rnd = rand::thread_rng();
    let range = Range::new(1u32, 2.pow(29));
    let num = range.ind_sample(&mut rnd);
    (num, base62::encode(&BigUint::from(num)))
}

fn main() {
    let config = Config { db_pool: establish_connection::<PgConnection>() };

    rocket::ignite()
        .mount("/",
               routes![
           handle_token, handle_project_token, create_shrt, update_shrt, delete_shrt, get_shrt
        ])
        .manage(config)
        .launch();
}


#[cfg(test)]
mod tests {
    use super::rocket;
    use test::*;
    use rocket::testing::MockRequest;
    use rocket::http::*;
    use rocket::http::Method::*;

    #[test]
    fn test_hello() {
        let config = super::Config { db_pool: super::establish_connection() };
        let rocket = rocket::ignite()
            .manage(config)
            .mount("/",
                   routes![super::handle_token, super::handle_project_token, super::create_shrt]);
        let mut req = MockRequest::new(Get, "/").header(Header::new("X-Forwarded-For", "1.2.3.4"));
        let mut response = req.dispatch_with(&rocket);
        assert_eq!(response.status(), Status::Ok);
        let body_str = response.body().and_then(|b| b.into_string());
        assert_eq!(body_str, Some("Done".into()));
    }

    #[test]
    fn test_create_random() {
        let config = super::Config { db_pool: super::establish_connection() };

        let rocket = rocket::ignite()
            .manage(config)
            .mount("/",
                   routes![super::handle_token,
                                super::handle_project_token,
                                super::create_shrt]);

        let mut req = MockRequest::new(Post, "/url")
            .header(ContentType::JSON)
            .body(r#"{ "long_url": "https://localhost:8000/long" }"#);

        let mut response = req.dispatch_with(&rocket);
        assert_eq!(response.status(), Status::Ok);
        let body = response.body().and_then(|b| b.into_string()).unwrap();
        assert!(body.contains(&"shrt_url"));
    }

    #[test]
    fn test_create_specified_token() {
        let config = super::Config { db_pool: super::establish_connection() };

        let rocket = rocket::ignite()
            .manage(config)
            .mount("/",
                   routes![super::handle_token,
                                super::handle_project_token,
                                super::create_shrt]);

        let mut req = MockRequest::new(Post, "/url")
            .header(ContentType::JSON)
            .body(r#"{ "long_url": "https://localhost:8000/long", "token": "test" }"#);

        let mut response = req.dispatch_with(&rocket);
        assert_eq!(response.status(), Status::Ok);
        let body = response.body().and_then(|b| b.into_string()).unwrap();
        assert!(body.contains(&"shrt_url"));
    }

    #[test]
    fn test_redirect_successful() {
        let config = super::Config { db_pool: super::establish_connection() };

        let rocket = rocket::ignite()
            .manage(config)
            .mount("/",
                   routes![super::handle_token,
                                super::handle_project_token,
                                super::create_shrt]);

        let mut req = MockRequest::new(Get, "/test")
            .header(ContentType::JSON)
            .header(hyper::header::Referer("steve".to_owned()))
            .header(Header::new("X-Forwarded-For", "1.2.3.4"))
            .header(Header::new("User-Agent", "smith"))
            .header(Header::new("Host", "localhost:8000"));

        let mut response = req.dispatch_with(&rocket);
        assert_eq!(response.status(), Status::TemporaryRedirect);

        let location: Vec<_> = response.header_values("location").collect();
        assert_eq!(location.len(), 1);
        assert!(location[0].ends_with(&"//google.com"));
    }

    #[test]
    fn test_redirect_failed_unknown() {
        let config = super::Config { db_pool: super::establish_connection() };

        let rocket = rocket::ignite()
            .manage(config)
            .mount("/",
                   routes![super::handle_token,
                                super::handle_project_token,
                                super::create_shrt]);

        let mut req = MockRequest::new(Get, "/bad")
            .header(ContentType::JSON)
            .header(hyper::header::Referer("steve".to_owned()))
            .header(Header::new("X-Forwarded-For", "1.2.3.4"))
            .header(Header::new("User-Agent", "smith"))
            .header(Header::new("Host", "localhost:8000"));

        let mut response = req.dispatch_with(&rocket);
        assert_eq!(response.status(), Status::TemporaryRedirect);

        let location: Vec<_> = response.header_values("location").collect();
        assert_eq!(location.len(), 1);
        assert!(location[0].ends_with(&"//localhost:8000"));
    }

    #[test]
    fn test_redirect_failed_disabled() {
        let config = super::Config { db_pool: super::establish_connection() };

        let rocket = rocket::ignite()
            .manage(config)
            .mount("/",
                   routes![super::handle_token,
                                super::handle_project_token,
                                super::create_shrt]);

        let mut req = MockRequest::new(Get, "/bad")
            .header(ContentType::JSON)
            .header(hyper::header::Referer("steve".to_owned()))
            .header(Header::new("X-Forwarded-For", "1.2.3.4"))
            .header(Header::new("User-Agent", "smith"))
            .header(Header::new("Host", "localhost:8000"));

        let mut response = req.dispatch_with(&rocket);
        assert_eq!(response.status(), Status::TemporaryRedirect);

        let location: Vec<_> = response.header_values("location").collect();
        assert_eq!(location.len(), 1);
        assert!(location[0].ends_with(&"//localhost:8000"));
    }

    #[test]
    fn gen_nonce() {
        use chrono::{UTC, TimeZone, DateTime};
        use std::time::Duration;

        let proto_id = 1u8;
        let timestamp = u32::max_value();
        let user_data = u16::max_value();
        let nonce = ((proto_id as u64) << 56);
        println!("{:b}", &nonce);

        let epoch: DateTime<UTC> = UTC.ymd(2000, 1, 1).and_hms(0, 0, 0);
        let diff: u32 = UTC::now().signed_duration_since(epoch).num_seconds() as u32;
        let nonce = nonce | ((diff as u64) << 24);
        println!("{:#?}", &epoch);
        println!("{:#?}", &diff);
        println!("{:b}", &nonce);
    }

    #[test]
    fn decoded() {
        println!("{}", super::base62::decode("Grqh9ncka"));
    }

    #[bench]
    fn many_tokens(b: &mut Bencher) {
        b.iter(|| {
            let val = black_box(1000000);
            for i in 0..val {
                let (num, token) = super::rand_token();
                // println!("{} {} {}", &num, &token)
            }
        })
    }
}
