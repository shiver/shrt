use rocket::{Outcome, State};
use rocket::request::{self, FromRequest, Request};

#[derive(Debug)]
pub struct ClientDetails {
    pub addr: Option<String>,
    pub agent: Option<String>,
    pub host: Option<String>,
    pub referrer: Option<String>,
}

impl ClientDetails {
    fn new() -> Self {
        ClientDetails {
            addr: None,
            agent: None,
            host: None,
            referrer: None,
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for ClientDetails {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<ClientDetails, ()> {
        let mut details = ClientDetails::new();
        if let Some(addr) = request.headers().get_one("x-forwarded-for") {
            details.addr = Some(addr.to_owned());
        } else {
            details.addr = Some(format!("{}", request.remote().unwrap()));
        };

        if let Some(agent) = request.headers().get_one("user-agent") {
            details.agent = Some(agent.to_owned());
        } else {
            details.agent = None;
        };

        if let Some(host) = request.headers().get_one("host") {
            details.host = Some(host.to_owned());
        } else {
            details.host = None;
        };

        if let Some(referrer) = request.headers().get_one("referer") {
            details.referrer = Some(referrer.to_owned());
        } else {
            details.referrer = None;
        };

        Outcome::Success(details)
    }
}
