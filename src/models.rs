use super::schema::{shrts, tracks, domains, projects};

use chrono::NaiveDateTime;


#[derive(Clone, Queryable, Identifiable, Associations)]
#[has_many(tracks)]
pub struct Shrt {
    pub id: i32,
    // pub uuid: Uuid,
    pub token: String,
    pub shrt_url: String,
    pub path_id: Option<i32>,
    pub domain_id: Option<i32>,
    pub redirect_url: String,
    pub redirect_status: Option<i32>,
    pub user_data: Option<i64>,
    pub active: bool,
    pub expire_after_inactivity: Option<i32>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>,
}

#[derive(Debug, Insertable)]
#[table_name="shrts"]
pub struct NewShrt<'a> {
    // pub uuid: &'a Uuid,
    pub token: &'a str,
    pub shrt_url: &'a str,
    pub path_id: Option<i32>,
    pub domain_id: Option<i32>,
    pub redirect_url: &'a str,
    pub redirect_status: Option<i32>,
    pub user_data: Option<i64>,
    pub active: bool,
    pub expire_after_inactivity: Option<i32>,
}


impl<'a> NewShrt<'a> {
    pub fn new() -> NewShrt<'a> {
        NewShrt {
            token: "",
            shrt_url: "",
            path_id: None,
            domain_id: None,
            redirect_url: "",
            redirect_status: None,
            user_data: None,
            active: true,
            expire_after_inactivity: Some(7776000),
        }
    }
}


#[derive(Queryable, Identifiable, Associations)]
#[belongs_to(Shrt)]
pub struct Track {
    pub id: i32,
    pub shrt_id: i32,
    pub token: String,
    pub host: Option<String>,
    pub shrt_url: String,
    pub redirect_url: String,
    pub injected_params: Option<String>,
    pub user_agent: Option<i32>,
    pub referrer: Option<String>,
    pub client_addr: Option<String>,
    pub gen_time: Option<i32>,
    pub created: Option<NaiveDateTime>,
}

#[derive(Debug, Insertable)]
#[table_name="tracks"]
pub struct NewTrack<'a> {
    pub shrt_id: i32,
    pub token: &'a str,
    pub host: Option<String>,
    pub shrt_url: &'a str,
    pub redirect_url: &'a str,
    pub user_agent: Option<String>,
    pub referrer: Option<String>,
    pub client_addr: Option<String>,
    pub gen_time: Option<i32>,
}

#[derive(Clone, Queryable, Identifiable, Associations)]
#[has_many(projects)]
pub struct Domain {
    pub id: i32,
    pub fqdn: String,
    pub fallback_url: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>,
}

#[derive(Debug, Insertable)]
#[table_name="domains"]
pub struct NewDomain {
    fqdn: String,
    fallback_url: Option<String>,
}

#[derive(Clone, Queryable, Identifiable, Associations)]
pub struct Project {
    pub id: i32,
    pub domain_id: i32,
    pub path: String,
    pub fallback_url: Option<String>,
    pub description: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>,
}

#[derive(Debug, Insertable)]
#[table_name="projects"]
pub struct NewProject {
    pub domain_id: i32,
    pub path: String,
    pub fallback_url: Option<String>,
    pub description: Option<String>,
}
