use std::env;
use std::time::Instant;

use diesel;
use diesel::prelude::*;
use diesel::pg::PgConnection;
use rocket::State;
use rocket::http::Status;
use rand;
use rand::distributions::{IndependentSample, Range};
use dotenv::dotenv;
use chrono::NaiveDateTime;

use schema;
use models::{Shrt, NewShrt, NewTrack, Domain, Project};
use ::Config;
use base62::{self, PrimInt};
use ::guard::ClientDetails;


error_chain!{
    errors {
        ShrtNotFound
        DomainNotFound
        ProjectNotFound
        LastActivityNotFound
    }
}


#[derive(Debug, Serialize, Deserialize)]
pub struct ShrtForm {
    long_url: String,
    token: Option<String>,
    domain: Option<String>,
    project: Option<String>,
    user_data: Option<String>,
}

type ActivityTuple = (i32, Option<i32>, Option<NaiveDateTime>);
pub struct ActivityType {
    pub id: i32,
    pub expires_after: Option<i32>,
    pub last_hit: Option<NaiveDateTime>,
}


pub fn get_shrt_from_token(token: &str,
                           path_id: Option<i32>,
                           config: &State<Config>)
                           -> Result<Shrt> {
    use schema::shrts;
    use schema::shrts::dsl;

    let conn = &*config.db_pool.clone().get().unwrap();
    let query = shrts::table.filter(shrts::token.eq(&token).and(shrts::active.eq(true)));
    if let Some(path_id) = path_id {
        let query = query.filter(shrts::path_id.eq(path_id));
    }

    let results = query.limit(1)
        .load::<Shrt>(conn)
        .chain_err(|| format!("Could not get shrt: {}", &token));

    if let Some(result) = results?.first() {
        return Ok(result.clone());
    }
    Err(ErrorKind::ShrtNotFound.into())
}

pub fn gen_shrt_from_form(frm: &ShrtForm, config: &State<Config>) -> Result<Shrt> {
    use schema::shrts;

    // TODO: User data
    // Add check here to ensure that doesn't exceed u32 bounds
    // let user_data = match frm.user_data {
    //    &Some(ref data) => Some(data.parse::<i64>().unwrap()),
    //    _ => None,
    // ;

    let mut path;
    let mut token;
    let mut shrt_url;
    let mut domain = String::new();
    let mut new_shrt = NewShrt::new();
    new_shrt.redirect_url = &frm.long_url;
    new_shrt.redirect_status = Some(Status::TemporaryRedirect.code as i32);

    if let Some(ref prj) = frm.project {
        match get_project_by_path(&prj, &config) {
            Ok(p) => {
                new_shrt.path_id = Some(p.id);
                path = format!("/{}", p.path).to_owned();
            }
            Err(e) => {
                return Err(e).chain_err(|| "Could not create shrt!");
            }
        };
    } else {
        path = String::from("");
    }

    if let Some(ref tkn) = frm.token {
        token = tkn.to_owned();
    } else {
        let (_, encoded) = rand_token();
        token = encoded.to_owned();
    }

    let conn = &*config.db_pool.clone().get().unwrap();
    if let Some(ref domain_name) = frm.domain {
        match get_domain_by_name(&domain_name, &conn) {
            Ok(d) => {
                new_shrt.domain_id = Some(d.id);
                domain = d.fqdn.to_owned();
            }
            Err(e) => {
                return Err(e).chain_err(|| "Could not create shrt! Domain not found.");
            }
        };
    } else {
        dotenv().ok();
        let default_domain = env::var("DEFAULT_DOMAIN")
            .expect("Could not determine default domain.");

        match get_domain_by_name(&default_domain, &conn) {
            Ok(d) => {
                new_shrt.domain_id = Some(d.id);
                domain = d.fqdn.to_owned();
            }
            Err(e) => {
                return Err(e).chain_err(|| "Could not create shrt!");
            }
        };
    };

    new_shrt.token = &token;
    shrt_url = format!("{}{}/{}", &domain, &path, &new_shrt.token).to_owned();
    new_shrt.shrt_url = &shrt_url;

    let result = diesel::insert(&new_shrt)
        .into(shrts::table)
        .get_result::<Shrt>(conn)
        .chain_err(|| "Error saving new shrt");

    Ok(result?.clone())
}

pub fn get_last_activity(shrt_id: i32, config: &State<Config>) -> Result<ActivityType> {
    use schema::{shrts, tracks};

    let conn = &*config.db_pool.clone().get().unwrap();
    let results = shrts::table.inner_join(tracks::table)
        .select((shrts::id, shrts::expire_after_inactivity, tracks::created))
        .filter(tracks::shrt_id.eq(shrt_id))
        .order(tracks::id.desc())
        .limit(1)
        .load::<ActivityTuple>(conn)
        .chain_err(|| format!("Could not get last activity: {}", shrt_id));

    if let Some(result) = results?.first() {
        return Ok(ActivityType {
            id: result.0,
            expires_after: result.1,
            last_hit: result.2,
        });
    }
    Err(ErrorKind::LastActivityNotFound.into())
}

pub fn get_domain_by_name(name: &str, conn: &PgConnection) -> Result<Domain> {
    use schema::domains;
    use schema::domains::dsl::*;

    let results = domains.filter(domains::fqdn.eq(&name))
        .limit(1)
        .load::<Domain>(conn)
        .chain_err(|| format!("Could not get domain: {}", &name));

    if let Some(result) = results?.first() {
        return Ok(result.clone());
    }

    Err(ErrorKind::DomainNotFound.into())
}

pub fn get_project_by_path(path: &str, config: &State<Config>) -> Result<Project> {
    use schema::projects;
    use schema::projects::dsl;

    let conn = &*config.db_pool.clone().get().unwrap();
    let results = dsl::projects.filter(projects::path.eq(&path))
        .limit(1)
        .load::<Project>(conn)
        .chain_err(|| format!("Could not get project: {}", &path));

    if let Some(result) = results?.first() {
        return Ok(result.clone());
    }

    Err(ErrorKind::ProjectNotFound.into())
}

pub fn store_track(now: &Instant,
                   url: &str,
                   client_details: &ClientDetails,
                   shrt: &Shrt,
                   config: &State<Config>)
                   -> Result<()> {
    use schema::tracks;

    let new_track = NewTrack {
        shrt_id: shrt.id,
        token: &shrt.token,
        host: client_details.host.to_owned(),
        shrt_url: &shrt.shrt_url,
        redirect_url: &url,
        user_agent: client_details.agent.to_owned(),
        referrer: client_details.referrer.to_owned(),
        client_addr: client_details.addr.to_owned(),
        gen_time: Some(now.elapsed().subsec_nanos() as i32),
    };

    let conn = &*config.db_pool.clone().get().unwrap();
    diesel::insert(&new_track)
        .into(tracks::table)
        .execute(conn)
        .chain_err(|| "Error saving new track");

    Ok(())
}

pub fn deactivate_shrt(token: &str, conn: &PgConnection) -> Result<()> {
    use schema::shrts;
    use schema::shrts::dsl::*;
    diesel::update(shrts.filter(token.eq(&token)))
        .set(active.eq(false))
        .execute(conn)
        .chain_err(|| "Could not deactivate shrt");

    Ok(())
}

pub fn get_hit_count(shrt_id: i32, config: &State<Config>) -> Result<i64> {
    use schema::{shrts, tracks};
    use schema::shrts::dsl::*;
    use diesel::expression::count;

    let conn = &*config.db_pool.clone().get().unwrap();
    tracks::table.select(count(tracks::id))
        .filter(tracks::shrt_id.eq(shrt_id))
        .first::<i64>(conn)
        .chain_err(|| "Could not get hit count")
}

// Consider moving this to its own space
fn rand_token() -> (u32, String) {
    let mut rnd = rand::thread_rng();
    let range = Range::new(1u32, 2.pow(29));
    let num = range.ind_sample(&mut rnd);
    (num, base62::encode(&base62::BigUint::from(num)))
}
