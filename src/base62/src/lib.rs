#![feature(test)]
extern crate num;
extern crate test;


pub use num::bigint::BigUint;
pub use num::traits::{pow, PrimInt, Zero};
use num::{Integer, ToPrimitive};


const BASE: u64 = 62;
const ALPHABET: [u8; BASE as usize] = [
    b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9',
    b'A', b'B', b'C', b'D', b'E', b'F', b'G', b'H', b'I', b'J',
    b'K', b'L', b'M', b'N', b'O', b'P', b'Q', b'R', b'S', b'T',
    b'U', b'V', b'W', b'X', b'Y', b'Z', b'a', b'b', b'c', b'd',
    b'e', b'f', b'g', b'h', b'i', b'j', b'k', b'l', b'm', b'n',
    b'o', b'p', b'q', b'r', b's', b't', b'u', b'v', b'w', b'x',
    b'y', b'z'];


pub fn encode(value: &BigUint) -> String {
    let mut bytes = Vec::new();
    let mut val = value.clone();

    let b = BigUint::from(BASE);
    while val > Zero::zero() {
        let ival = val.mod_floor(&b).to_usize().unwrap();
        bytes.push(ALPHABET[ival]);
        val = val / &b;
    }
    bytes.reverse();
    String::from_utf8(bytes).unwrap()
}

pub fn decode(value: &str) -> BigUint {
    let mut result: BigUint = Zero::zero();
    let bytes = value.as_bytes();
    for (idx, c) in bytes.iter().rev().enumerate() {
        let num = pow(BigUint::from(BASE), idx);
        let c_idx = ALPHABET.binary_search(c).unwrap();
        let num = BigUint::from(c_idx as u64) * num;
        result = result + num;
    }
    result
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::{Bencher, black_box};

    #[test]
    fn it_works() {
        println!("{}", super::decode("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"))
    }

    #[bench]
    fn many_encodes(b: &mut Bencher) {
        let total = black_box(1000) as u64;
        let vals: Vec<BigUint> = (0..total).map(|v| BigUint::from(v)).collect();

        b.iter(move || {
            for val in &vals {
                let result = super::encode(&val);
                //println!("{}", &result);
            }
        })
    }
}
